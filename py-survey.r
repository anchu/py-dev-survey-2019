
require(ggplot2)
theme_set(theme(text = element_text(size = getOption("base_size"), family = "Roboto Condensed")))

psf = read.csv("python_psf_external_19.csv")

misvals = vector("numeric", NCOL(psf))

for (i in seq_len(NCOL(psf))) {
    misvals[i] = sum(is.na(psf[, i])) / NROW(psf)
}

ggplot(data.frame(misvals)) +
    aes(misvals) +
    geom_histogram(color = "white", bins = 30)

sum(misvals > 0.75) / NROW(misvals) * 100 # 80% variables have rate of missing values > 75%
## This data set is not usable
